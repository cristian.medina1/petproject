package com.fungynature.product.commandhandle;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fungynature.product.command.SaveProductCommand;
import com.fungynature.product.command.handle.SaveProductCommandHandle;
import com.fungynature.product.common.Response;
import com.fungynature.product.controller.dto.SaveProductReq;
import com.fungynature.product.controller.dto.SaveProductResp;
import com.fungynature.product.entities.Product;
import com.fungynature.product.exception.BussinesRuleException;
import com.fungynature.product.repository.ProductRepository;

@ExtendWith(MockitoExtension.class)
class SaveProductCommandHandleTest {

	@Mock
	ProductRepository repository;
	
	@InjectMocks
	SaveProductCommandHandle commandHandle;
	
	@ParameterizedTest
	@CsvSource({"1", "2", "3", "4", "5"})
	void validate_AmountBag_To_SaveProductResp_Is_Equals_To_AmountBagRequest_When_Save_AmountBag_To_Request_Is_Positive(int amount) throws BussinesRuleException {

		Date date = new Date();

		SaveProductReq request = new SaveProductReq();
		request.setAmountBag(amount);
		request.setDate(date);
		SaveProductCommand commandRequest = new SaveProductCommand(request);
		
		Product product = new Product();
		product.setAmountBag(amount);
		product.setDate(date);
		
		Mockito.when(repository.save(product)).thenReturn(product);
		
		Response<SaveProductResp> response = commandHandle.handle(commandRequest);
		
		assertEquals(amount, response.getBody().getProduct().getAmountBag());

	}
	
	@Test
	void validate_BussinesMessageError_When_AmountBagRequest_Is_Negative() throws BussinesRuleException {
		
		SaveProductCommandHandle commandHandleExcepction = new SaveProductCommandHandle();
		
		Date date = new Date();

		SaveProductReq request = new SaveProductReq();
		request.setAmountBag(-1);
		request.setDate(date);
		SaveProductCommand commandRequest = new SaveProductCommand(request);
		
		Product product = new Product();
		product.setAmountBag(-1);
		product.setDate(date);
		
		BussinesRuleException exception = assertThrows(BussinesRuleException.class, () -> {
			commandHandleExcepction.handle(commandRequest);
		});
		
		assertEquals("Parametro no valido", exception.getMessage());
	}
	
	
	@Test
	void validate_BussinesCodeError_When_AmountBagRequest_Is_Negative() throws BussinesRuleException {
		
		SaveProductCommandHandle commandHandleExcepction = new SaveProductCommandHandle();
		
		Date date = new Date();

		SaveProductReq request = new SaveProductReq();
		request.setAmountBag(-1);
		request.setDate(date);
		SaveProductCommand commandRequest = new SaveProductCommand(request);
		
		Product product = new Product();
		product.setAmountBag(-1);
		product.setDate(date);
		
		BussinesRuleException exception = assertThrows(BussinesRuleException.class, () -> {
			commandHandleExcepction.handle(commandRequest);
		});
		
		assertEquals("PR-1001", exception.getCode());
	}
}
