package com.fungynature.product.commandhandle;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fungynature.product.command.GetProductsCommand;
import com.fungynature.product.command.handle.GetProductsCommandHandle;
import com.fungynature.product.common.Response;
import com.fungynature.product.controller.dto.GetProductsResp;
import com.fungynature.product.entities.Product;
import com.fungynature.product.exception.BussinesRuleException;
import com.fungynature.product.repository.ProductRepository;

@ExtendWith(MockitoExtension.class)
class GetProductsCommandHandleTest {
	
	@Mock
	ProductRepository repository;
	
	@InjectMocks
	GetProductsCommandHandle commandHandle;
	
	@Test
	void evaluate_Response_Not_Null_When_Exist_Product_In_ListProduct() throws BussinesRuleException {
		Product product = new Product();
		product.setAmountBag(1);
		product.setDate(new Date());
		Mockito.when(repository.findAll()).thenReturn(Arrays.asList(product));
		
		GetProductsCommand commandRequest = new GetProductsCommand();
		Response<GetProductsResp> response = commandHandle.handle(commandRequest);
		assertNotNull(response);
	}
	
	@Test
	void evaluate_Response_Is_Not_Empty_When_Exist_Product_Exist_In_ListProduct() throws BussinesRuleException {
		Product product = new Product();
		Mockito.when(repository.findAll()).thenReturn(Arrays.asList(product));
		
		GetProductsCommand commandRequest = new GetProductsCommand();
		Response<GetProductsResp> response = commandHandle.handle(commandRequest);
		assertFalse(response.getBody().getProducts().isEmpty());
	}
	
	@Test
	void evaluate_Size_Array_Is_One_To_ListProduct_To_GetProductsResponse() throws BussinesRuleException {
		Product product = new Product();
		Mockito.when(repository.findAll()).thenReturn(Arrays.asList(product));

		GetProductsCommand commandRequest = new GetProductsCommand();
		Response<GetProductsResp> response = commandHandle.handle(commandRequest);
		assertEquals(1, response.getBody().getProducts().size());
	}
	
	@Test
	void validate_CodeError_To_GetProductsResponse_When_Not_Exist_Product_In_ListProducts() throws BussinesRuleException {
		GetProductsCommand commandRequest = new GetProductsCommand();
		List<Product> products = new ArrayList<>();
		
		Mockito.when(repository.findAll()).thenReturn(products);
		
		BussinesRuleException exception = assertThrows(BussinesRuleException.class, () -> {
			commandHandle.handle(commandRequest);
		});
		
		assertEquals("PR-1003", exception.getCode());
	}
	
	@Test
	void validate_MessageeError_To_GetProductsResponse_When_Not_Exist_Product_In_ListProduct() throws BussinesRuleException {
		GetProductsCommand commandRequest = new GetProductsCommand();
		List<Product> products = new ArrayList<>();
		
		Mockito.when(repository.findAll()).thenReturn(products);
		
		BussinesRuleException exception = assertThrows(BussinesRuleException.class, () -> {
			commandHandle.handle(commandRequest);
		});
		
		assertEquals("Lista sin contenido", exception.getMessage());
	}
}
