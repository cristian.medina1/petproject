package com.fungynature.product.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fungynature.product.controller.dto.GetProductsReq;
import com.fungynature.product.controller.dto.SaveProductReq;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class ProductRestControllerTest {

	@LocalServerPort
	int randomServerPort;

	private <T> ResponseEntity<String> getclient(String path, T requestParameter) throws URISyntaxException {

		TestRestTemplate testRestTemplate = new TestRestTemplate();

		final String baseUrl = "http://localhost:" + randomServerPort + path;
		URI uri = new URI(baseUrl);

		HttpHeaders headers = new HttpHeaders();
		// headers.set("Authorization", "TokenTest");

		HttpEntity<T> request = new HttpEntity<>(requestParameter);

		ResponseEntity<String> result = testRestTemplate.exchange(uri, HttpMethod.POST, request, String.class);

		return result;
	}
	
	private <T> ResponseEntity<String> getclientTwo(String path) throws URISyntaxException {

		TestRestTemplate testRestTemplate = new TestRestTemplate();

		final String baseUrl = "http://localhost:" + randomServerPort + path;
		URI uri = new URI(baseUrl);

		HttpHeaders headers = new HttpHeaders();
		// headers.set("Authorization", "TokenTest");

		HttpEntity<T> request = new HttpEntity<>(headers);

		ResponseEntity<String> result = testRestTemplate.exchange(uri, HttpMethod.GET, request, String.class);

		return result;
	}

	@ParameterizedTest
	@CsvSource({"1", "2", "3", "4", "5"})
	void saveProductToVerifyResponseCreate(int amountBag) throws URISyntaxException {

		SaveProductReq req = new SaveProductReq();
		req.setAmountBag(amountBag);
		ResponseEntity<String> result = getclient("/product/nuevo", req);

		// Verify request succeed
		assertEquals(HttpStatus.CREATED, result.getStatusCode());
	}

	@ParameterizedTest
	@CsvSource({"-1", "-22", "-3", "-40", "-5"})
	void saveProductToVerifyResponseBadRequest(int amountBag) throws URISyntaxException {
		SaveProductReq req = new SaveProductReq();
		req.setAmountBag(amountBag);
		ResponseEntity<String> result = getclient("/product/nuevo", req);

		// Verify request succeed
		assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
	}
	
	@Test
	void getProductsToVerifyResponseBadRequestByNoExitsProducts() throws URISyntaxException {
		GetProductsReq req = new GetProductsReq();
		
		ResponseEntity<String> result = getclientTwo("/product/products");

		// Verify request succeed
		assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
	}

}
