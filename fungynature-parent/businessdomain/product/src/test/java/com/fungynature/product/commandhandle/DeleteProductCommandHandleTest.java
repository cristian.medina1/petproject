package com.fungynature.product.commandhandle;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fungynature.product.command.DeleteProductCommand;
import com.fungynature.product.command.handle.DeleteProductCommandHandle;
import com.fungynature.product.common.Response;
import com.fungynature.product.controller.dto.DeleteProductReq;
import com.fungynature.product.controller.dto.DeleteProductResp;
import com.fungynature.product.entities.Product;
import com.fungynature.product.exception.BussinesRuleException;
import com.fungynature.product.repository.ProductRepository;

@ExtendWith(MockitoExtension.class)
class DeleteProductCommandHandleTest {
	
	@InjectMocks
	DeleteProductCommandHandle commandHandle;
	
	@Mock
	ProductRepository repository;
	
	@Test
	void validate_Message_To_Response_When_Delete_Product_Exist_In_ListProduct() throws BussinesRuleException {
		
		DeleteProductReq request = new DeleteProductReq();
		request.setProductId(1);
		
		Product product = new Product();
		product.setId(1L);
		
		Product product2 = new Product();
		product2.setId(2L);
		
		Product product3 = new Product();
		product3.setId(3L);
		
		Mockito.when(repository.findAll()).thenReturn(Arrays.asList(product, product2, product3));
		
		DeleteProductCommand commandRequest = new DeleteProductCommand(request);
		Response<DeleteProductResp> response = commandHandle.handle(commandRequest);
		
		assertEquals("Producto Eliminado", response.getBody().getMessage());
	
	}
	
	
	@Test
	void validate_Message_To_BussinesRuleException_When_Delete_Product_Not_Exist_In_ListProduct() throws BussinesRuleException {
		
		DeleteProductReq request = new DeleteProductReq();
		request.setProductId(4);
		
		Product product = new Product();
		product.setId(1L);
		
		Product product2 = new Product();
		product2.setId(2L);
		
		Product product3 = new Product();
		product3.setId(3L);
		
		Mockito.when(repository.findAll()).thenReturn(Arrays.asList(product, product2, product3));
		
		DeleteProductCommand commandRequest = new DeleteProductCommand(request);
		
		BussinesRuleException exception = assertThrows(BussinesRuleException.class, () -> {
			commandHandle.handle(commandRequest);
		});
		
		assertEquals("Id no encontrado", exception.getMessage());
	
	}
	
	@Test
	void validate_Code_To_BussinesRuleException_When_Delete_Product_Not_Exist_In_ListProduct() throws BussinesRuleException {
		
		DeleteProductReq request = new DeleteProductReq();
		request.setProductId(4);
		
		Product product = new Product();
		product.setId(1L);
		
		Product product2 = new Product();
		product2.setId(2L);
		
		Product product3 = new Product();
		product3.setId(3L);
		
		Mockito.when(repository.findAll()).thenReturn(Arrays.asList(product, product2, product3));
		
		DeleteProductCommand commandRequest = new DeleteProductCommand(request);
		
		BussinesRuleException exception = assertThrows(BussinesRuleException.class, () -> {
			commandHandle.handle(commandRequest);
		});
		
		assertEquals("PR-1004", exception.getCode());
	
	}
	

}
