package com.fungynature.product.common;

import java.io.Serializable;

import com.fungynature.product.exception.BussinesRuleException;

public interface ICommandHandler<R extends Serializable, C extends ICommandRequest<?>> {
	Response<R> handle(C commandRequest) throws BussinesRuleException;
}
