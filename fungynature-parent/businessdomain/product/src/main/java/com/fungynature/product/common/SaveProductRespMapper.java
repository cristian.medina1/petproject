package com.fungynature.product.common;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.fungynature.product.dto.SaveProductResp;
import com.fungynature.product.entities.Product;

@Mapper(componentModel = "spring")
public interface SaveProductRespMapper {

	@Mapping(source = "id", target = "productId")
	SaveProductResp productToSaveProductResp(Product source);
}
