package com.fungynature.product.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class DateRange implements Serializable{
	private Date dateInit;
	private Date dateEnd;
}
