package com.fungynature.product.controller.dto;

import lombok.Data;

@Data
public class DeleteProductReq {

	private long productId;
}
