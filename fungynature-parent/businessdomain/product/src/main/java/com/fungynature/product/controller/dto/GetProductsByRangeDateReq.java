package com.fungynature.product.controller.dto;

import java.io.Serializable;

import com.fungynature.product.dto.DateRange;

import lombok.Data;

@Data
public class GetProductsByRangeDateReq implements Serializable{

	private DateRange dateRange;
}
