package com.fungynature.product.command.handle;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.fungynature.product.command.GetProductsByRangeDateCommand;
import com.fungynature.product.common.ICommandHandler;
import com.fungynature.product.common.Response;
import com.fungynature.product.common.Status;
import com.fungynature.product.controller.dto.GetProductsByRangeDateResp;
import com.fungynature.product.entities.Product;
import com.fungynature.product.exception.BussinesRuleException;
import com.fungynature.product.repository.ProductRepository;

@Component
public class GetProductsByRangeDateCommandHandle
		implements ICommandHandler<GetProductsByRangeDateResp, GetProductsByRangeDateCommand> {

	@Autowired
	ProductRepository repository;

	@Override
	public Response<GetProductsByRangeDateResp> handle(GetProductsByRangeDateCommand commandRequest)
			throws BussinesRuleException {

		List<Product> products = getProductsByRangeDate(commandRequest);

		return getResponse(products);
	}

	private Status setStatus() {
		Status status = new Status();
		status.setCode("0000");
		status.setDescription("Ejecucion correcta");

		return status;
	}

	private void validateContentListProduct(List<Product> products) throws BussinesRuleException {
		if (products.isEmpty() || products == null) {
			BussinesRuleException exception = new BussinesRuleException("PR-1003", "Lista sin contenido",
					HttpStatus.NO_CONTENT);
			throw exception;
		}
	}

	private List<Product> getProductsByRangeDate(GetProductsByRangeDateCommand commandRequest)
			throws BussinesRuleException {
		List<Product> productsFilters = new ArrayList<>();
		
		List<Product> products = repository.findAll();

		validateContentListProduct(products);

		for (Product product : products) {

			if (product.getDate().after(commandRequest.getDateRange().getDateInit())
					&& product.getDate().before(commandRequest.getDateRange().getDateEnd())) {
				productsFilters.add(product);
			}
		}

		return productsFilters;

	}

	private Response<GetProductsByRangeDateResp> getResponse(List<Product> products) {
		Response<GetProductsByRangeDateResp> response = new Response<>();
		
		GetProductsByRangeDateResp getProductsByRangeDateResp = new GetProductsByRangeDateResp();
		getProductsByRangeDateResp.setProducts(products);
		
		response.setBody(getProductsByRangeDateResp);
		response.setStatus(setStatus());
		
		return response;
	}
}
