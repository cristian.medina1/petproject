package com.fungynature.product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fungynature.product.command.DeleteProductCommand;
import com.fungynature.product.command.GetProductsByRangeDateCommand;
import com.fungynature.product.command.GetProductsCommand;
import com.fungynature.product.command.SaveProductCommand;
import com.fungynature.product.common.ICommandBus;
import com.fungynature.product.common.Response;
import com.fungynature.product.controller.dto.DeleteProductReq;
import com.fungynature.product.controller.dto.DeleteProductResp;
import com.fungynature.product.controller.dto.GetProductsByRangeDateReq;
import com.fungynature.product.controller.dto.GetProductsByRangeDateResp;
import com.fungynature.product.controller.dto.GetProductsResp;
import com.fungynature.product.controller.dto.SaveProductReq;
import com.fungynature.product.controller.dto.SaveProductResp;
import com.fungynature.product.exception.BussinesRuleException;

@RestController
@RequestMapping("/product")
public class ProductRestController {

	@Autowired
	private ICommandBus commandBus;

	@DeleteMapping
	public ResponseEntity<Response<DeleteProductResp>> deleteProduct(DeleteProductReq request)
			throws BussinesRuleException {
		return new ResponseEntity<>(commandBus.handle(new DeleteProductCommand(request)), HttpStatus.OK);
	}

	@GetMapping("/products")
	public ResponseEntity<Response<GetProductsResp>> getProducts() throws BussinesRuleException {
		return new ResponseEntity<>(commandBus.handle(new GetProductsCommand()), HttpStatus.OK);
	}

	@PostMapping("/nuevo")
	public ResponseEntity<Response<SaveProductResp>> saveProduct2(@RequestBody SaveProductReq request)
			throws BussinesRuleException {
		return new ResponseEntity<>(commandBus.handle(new SaveProductCommand(request)), HttpStatus.CREATED);
		// var uno = commandBus.handle(new SaveProductCommand(request));
	}

	@PostMapping(path="/date", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<GetProductsByRangeDateResp>> getProductsByRangeDate(
			@RequestBody GetProductsByRangeDateReq request) throws BussinesRuleException {
		return new ResponseEntity<>(commandBus.handle(new GetProductsByRangeDateCommand(request)), HttpStatus.OK);
	}
}
