package com.fungynature.product.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.GenericTypeResolver;

public class Registry {

	private static final Logger LOG = LoggerFactory.getLogger(Registry.class);
	private Map<Class<? extends ICommandRequest>, CommandProvider<?>> providerMap = new HashMap<>();
	private ApplicationContext applicationContext;

	@Autowired
	public Registry(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
		String[] names = applicationContext.getBeanNamesForType(ICommandHandler.class);
		for (String name : names) {
			register(applicationContext, name);
		}
	}

	
	private void register(ApplicationContext applicationContext, String name) {
		LOG.info("Iniciando el registro del commando: {} ", name);
		Class<ICommandHandler<?, ?>> handlerClass = (Class<ICommandHandler<?, ?>>) applicationContext.getType(name);
		Class<?>[] generics = GenericTypeResolver.resolveTypeArguments(handlerClass, ICommandHandler.class);
		Class<? extends ICommandRequest> commandType = (Class<? extends ICommandRequest>) generics[1];
		providerMap.put(commandType, new CommandProvider(handlerClass));
		LOG.info("Finalizando el registro del commando: {} ", name);
	}

	public <R extends Serializable, C extends ICommandRequest<R>> ICommandHandler<R, C> get(Class<C> commandClass) {
		return (ICommandHandler<R, C>) providerMap.get(commandClass).get(this.applicationContext);
	}
}
