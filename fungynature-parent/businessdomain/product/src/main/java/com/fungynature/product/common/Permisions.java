package com.fungynature.product.common;

import lombok.Generated;

public class Permisions {

	private String pagePermission;
	private String actionPermision;

	public Permisions() {
	}

	@Generated
	public String getPagePermission() {
		return this.pagePermission;
	}

	@Generated
	public String getActionPermision() {
		return this.actionPermision;
	}

	@Generated
	public void setPagePermission(final String pagePermission) {
		this.pagePermission = pagePermission;
	}

	@Generated
	public void setActionPermision(final String actionPermision) {
		this.actionPermision = actionPermision;
	}

}
