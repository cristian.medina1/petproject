package com.fungynature.product.command;

import com.fungynature.product.common.ICommandRequest;
import com.fungynature.product.controller.dto.GetProductsByRangeDateReq;
import com.fungynature.product.dto.DateRange;

import lombok.Data;

@Data
public class GetProductsByRangeDateCommand implements ICommandRequest<GetProductsByRangeDateReq>{

	private DateRange dateRange;
	
	public GetProductsByRangeDateCommand(GetProductsByRangeDateReq req) {
		this.dateRange = req.getDateRange();
	}
}
