package com.fungynature.product.command.handle;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.fungynature.product.command.GetProductsCommand;
import com.fungynature.product.common.ICommandHandler;
import com.fungynature.product.common.Response;
import com.fungynature.product.common.Status;
import com.fungynature.product.controller.dto.GetProductsResp;
import com.fungynature.product.entities.Product;
import com.fungynature.product.exception.BussinesRuleException;
import com.fungynature.product.repository.ProductRepository;

@Component
public class GetProductsCommandHandle implements ICommandHandler<GetProductsResp, GetProductsCommand> {

	@Autowired
	ProductRepository repository;

	@Override
	public Response<GetProductsResp> handle(GetProductsCommand commandRequest) throws BussinesRuleException {

		List<Product> products = repository.findAll();
		
		validateContentListProduct(products);

		return getResponse(products);
	}

	private void validateContentListProduct(List<Product> products) throws BussinesRuleException {
		if (products.isEmpty() || products == null) {
			BussinesRuleException exception = new BussinesRuleException("PR-1003", "Lista sin contenido",
					HttpStatus.NO_CONTENT);
			throw exception;
		}
	}

	private Status setStatus() {
		Status status = new Status();
		status.setCode("0000");
		status.setDescription("Ejecucion correcta");

		return status;
	}
	
	private Response<GetProductsResp> getResponse(List<Product> products) {
		Response<GetProductsResp> response = new Response<>();
		
		GetProductsResp resp = new GetProductsResp();
		resp.setProducts(products);
		
		response.setBody(resp);
		response.setStatus(setStatus());
		return response;
	}

}
