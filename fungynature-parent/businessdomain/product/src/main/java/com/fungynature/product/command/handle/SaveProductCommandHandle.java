package com.fungynature.product.command.handle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.fungynature.product.command.SaveProductCommand;
import com.fungynature.product.common.ICommandHandler;
import com.fungynature.product.common.Response;
import com.fungynature.product.common.Status;
import com.fungynature.product.controller.dto.SaveProductResp;
import com.fungynature.product.entities.Product;
import com.fungynature.product.exception.BussinesRuleException;
import com.fungynature.product.repository.ProductRepository;

@Component
public class SaveProductCommandHandle implements ICommandHandler<SaveProductResp, SaveProductCommand> {

	@Autowired
	ProductRepository repository;

	@Override
	public Response<SaveProductResp> handle(SaveProductCommand commandRequest) throws BussinesRuleException {

		validateAmountBagProduct(commandRequest);
		Product product = saveProduct(mapperProductDTOToProduct(commandRequest));
	
		return  getResponse(product);
	}

	private Status setStatus() {
		Status status = new Status();
		status.setCode("0000");
		status.setDescription("Ejecucion correcta");

		return status;
	}

	private void validateAmountBagProduct(SaveProductCommand commandRequest) throws BussinesRuleException {
		if (commandRequest.getAmountBag() < 0) {
			BussinesRuleException exception = new BussinesRuleException("PR-1001", "Parametro no valido",
					HttpStatus.BAD_REQUEST);
			throw exception;
		} 
	}
	
	private Product saveProduct(Product product) {
		return repository.save(product);
	}

	private Response<SaveProductResp> getResponse(Product product) {
		
		Response<SaveProductResp> response = new Response<>();
		
		SaveProductResp saveProductResp = new SaveProductResp();
		
		saveProductResp.setProduct(product);
		
		response.setBody(saveProductResp);

		response.setStatus(setStatus());

		return response;
	}
	
	private Product mapperProductDTOToProduct(SaveProductCommand commandRequest) {
		Product product = new Product();
		product.setAmountBag(commandRequest.getAmountBag());
		product.setDate(commandRequest.getDate());
		
		return product;
	}

}
