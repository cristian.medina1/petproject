package com.fungynature.product.common;

import java.io.Serializable;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response<T> {

	private T body;
	private Status status;
	private Permisions permision;

	@Generated
	public static <T> Response.ResponseBuilder<T> builder() {
		return new Response.ResponseBuilder();
	}

	@Generated
	public Response(final T body, final Status status, final Permisions permision) {
		this.body = body;
		this.status = status;
		this.permision = permision;
	}

	@Generated
	public Response() {
	}

	@Generated
	public T getBody() {
		return this.body;
	}

	@Generated
	public Status getStatus() {
		return this.status;
	}

	@Generated
	public Permisions getPermision() {
		return this.permision;
	}

	@Generated
	public void setBody(final T body) {
		this.body = body;
	}

	@Generated
	public void setStatus(final Status status) {
		this.status = status;
	}

	@Generated
	public void setPermision(final Permisions permision) {
		this.permision = permision;
	}

	@Generated
	public static class ResponseBuilder<T> {
		@Generated
		private T body;
		@Generated
		private Status status;
		@Generated
		private Permisions permision;

		@Generated
		ResponseBuilder() {
		}

		@Generated
		public Response.ResponseBuilder<T> body(final T body) {
			this.body = body;
			return this;
		}

		@Generated
		public Response.ResponseBuilder<T> status(final Status status) {
			this.status = status;
			return this;
		}

		@Generated
		public Response.ResponseBuilder<T> permision(final Permisions permision) {
			this.permision = permision;
			return this;
		}

		@Generated
		public Response<T> build() {
			return new Response(this.body, this.status, this.permision);
		}

		@Generated
		public String toString() {
			return "Response.ResponseBuilder(body=" + this.body + ", status=" + this.status + ", permision="
					+ this.permision + ")";
		}
	}
}
