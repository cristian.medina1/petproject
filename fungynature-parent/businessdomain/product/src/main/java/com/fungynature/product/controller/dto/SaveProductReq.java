package com.fungynature.product.controller.dto;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "Este modelo representa el request para la peticion de guardar un registro de producto")
public class SaveProductReq {

	@ApiModelProperty(name = "amountBag", required = true, example = "1", value = "Unique Id of customer taht represent the owner of invoice")
	private int amountBag;
	private Date date;
	// private List<?> bags;
}
