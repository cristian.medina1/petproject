package com.fungynature.product.command.handle;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.fungynature.product.command.DeleteProductCommand;
import com.fungynature.product.common.ICommandHandler;
import com.fungynature.product.common.Response;
import com.fungynature.product.common.Status;
import com.fungynature.product.controller.dto.DeleteProductResp;
import com.fungynature.product.entities.Product;
import com.fungynature.product.exception.BussinesRuleException;
import com.fungynature.product.repository.ProductRepository;

@Component
public class DeleteProductCommandHandle implements ICommandHandler<DeleteProductResp, DeleteProductCommand> {

	@Autowired
	ProductRepository repository;

	@Override
	public Response<DeleteProductResp> handle(DeleteProductCommand commandRequest) throws BussinesRuleException {

		Product product = getProductById(commandRequest);
		deleteProduct(product);
		
		return getResponse();
	}

	private Status setStatus() {
		Status status = new Status();
		status.setCode("0000");
		status.setDescription("Ejecucion correcta");

		return status;
	}

	private Product getProductById(DeleteProductCommand commandRequest) throws BussinesRuleException {
		List<Product> products = repository.findAll();

		return products.stream().filter(product -> product.getId().equals(commandRequest.getProductId())).findFirst()
				.orElseThrow(() -> new BussinesRuleException("PR-1004", "Id no encontrado", HttpStatus.NO_CONTENT));
	}

	private void deleteProduct(Product product) {
		repository.delete(product);
	}
	
	private Response<DeleteProductResp> getResponse(){
		DeleteProductResp deleteProductResp = new DeleteProductResp();
		deleteProductResp.setMessage("Producto Eliminado");
		
		Response<DeleteProductResp> response = new Response<>();

		response.setBody(deleteProductResp);
		response.setStatus(setStatus());
		
		return response;
	}

}
