package com.fungynature.product.common;

import org.springframework.context.ApplicationContext;

public class CommandProvider<H extends ICommandHandler<?, ?>> {

    private final Class<H> type;
    CommandProvider(Class<H> type) {
        this.type = type;
    }

    public H get(ApplicationContext applicationContext) {
        return applicationContext.getBean(type);
    }
}
