package com.fungynature.product.controller.dto;

import java.io.Serializable;
import java.util.List;

import com.fungynature.product.entities.Product;

import lombok.Data;

@Data
public class GetProductsByRangeDateResp implements Serializable{

	private List<Product> products;
}
