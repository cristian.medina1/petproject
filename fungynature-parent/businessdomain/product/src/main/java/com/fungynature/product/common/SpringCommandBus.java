package com.fungynature.product.common;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;

import com.fungynature.product.exception.BussinesRuleException;

public class SpringCommandBus implements ICommandBus {
	private final Registry registry;

	@Autowired
	public SpringCommandBus(Registry registry) {
		this.registry = registry;
	}

	@Override
	public <R extends Serializable, C extends ICommandRequest<?>> Response<R> handle(C command) throws BussinesRuleException {
		ICommandHandler<R, C> commandHandler = registry.get(command.getClass());
		return commandHandler.handle(command);
	}
}