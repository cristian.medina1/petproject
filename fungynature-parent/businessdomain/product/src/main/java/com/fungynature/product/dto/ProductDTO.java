package com.fungynature.product.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class ProductDTO implements Serializable {

	private long productId;
	private int amountBag;
	private Date date;
}
