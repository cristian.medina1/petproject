package com.fungynature.product.controller.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "Este modelo representa el response de la peticion para eliminar un producto")
public class DeleteProductResp implements Serializable {

	private String message;
}
