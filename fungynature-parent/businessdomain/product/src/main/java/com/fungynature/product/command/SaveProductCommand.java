package com.fungynature.product.command;

import java.util.Date;

import com.fungynature.product.common.ICommandRequest;
import com.fungynature.product.controller.dto.SaveProductReq;

import lombok.Data;

@Data
public class SaveProductCommand implements ICommandRequest<SaveProductReq> {

	private int amountBag;
	private Date date;

	public SaveProductCommand(SaveProductReq req) {
		this.amountBag = req.getAmountBag();
		this.date = req.getDate();
	}

}
