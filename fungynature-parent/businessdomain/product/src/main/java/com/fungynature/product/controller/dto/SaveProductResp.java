package com.fungynature.product.controller.dto;

import java.io.Serializable;

import com.fungynature.product.entities.Product;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "Este modelo representa la respuesta despues de solicitar guardar un registro de producto")
public class SaveProductResp implements Serializable {

	private Product product;
	// private List<?> bags;
}
