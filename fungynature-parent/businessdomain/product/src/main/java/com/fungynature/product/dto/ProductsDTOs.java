package com.fungynature.product.dto;

import java.util.List;

import com.fungynature.product.entities.Product;

import lombok.Data;

@Data
public class ProductsDTOs {

	private List<Product> productsDTOs;
}
