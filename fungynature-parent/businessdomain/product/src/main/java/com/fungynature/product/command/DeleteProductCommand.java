package com.fungynature.product.command;

import com.fungynature.product.common.ICommandRequest;
import com.fungynature.product.controller.dto.DeleteProductReq;

import lombok.Data;

@Data
public class DeleteProductCommand implements ICommandRequest<DeleteProductReq> {

	private Long productId;

	public DeleteProductCommand(DeleteProductReq request) {
		this.productId = request.getProductId();
	}
}
