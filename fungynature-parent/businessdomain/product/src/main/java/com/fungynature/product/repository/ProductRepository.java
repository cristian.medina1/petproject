package com.fungynature.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fungynature.product.entities.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
