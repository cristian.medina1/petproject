package com.fungynature.product.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.fungynature.product.common.StandarizedApiExceptionResponse;

@RestControllerAdvice
public class ApiExceptionHandler {

	@ResponseBody
	@ExceptionHandler(value = { Exception.class })
	public ResponseEntity<StandarizedApiExceptionResponse> handleNoContentException(Exception ex) {
		StandarizedApiExceptionResponse response = new StandarizedApiExceptionResponse("Error de conexion",
				"erorr-1024", ex.getMessage());
		return new ResponseEntity<>(response, HttpStatus.PARTIAL_CONTENT);
	}

	@ResponseBody
	@ExceptionHandler(value = { BussinesRuleException.class })
	public ResponseEntity<StandarizedApiExceptionResponse> handleBussinesRuleException(BussinesRuleException ex) {
		StandarizedApiExceptionResponse response = new StandarizedApiExceptionResponse(
				ex.getMessage(), ex.getCode(), ex.getMessage());
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	@ResponseBody
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<StandarizedApiExceptionResponse> handleNotFoundException(NotFoundException ex) {
		StandarizedApiExceptionResponse response = new StandarizedApiExceptionResponse(
				"No se encontro el id del producto", ex.getCode(), ex.getMessage());
		return new ResponseEntity<>(response, ex.getHttpStatus());
	}
}
