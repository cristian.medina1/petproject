package com.fungynature.product.common;

import org.mapstruct.Mapper;

import com.fungynature.product.dto.SaveProductReq;
import com.fungynature.product.entities.Product;

@Mapper(componentModel = "spring")
public interface SaveProductReqMapper {

	Product savedProductReqToProduct(SaveProductReq source);

}
