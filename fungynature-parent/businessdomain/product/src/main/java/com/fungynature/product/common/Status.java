package com.fungynature.product.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Status {

	private String code;
	private String description;
}
